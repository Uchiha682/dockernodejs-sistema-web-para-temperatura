const express = require('express');
const bodyParser = require('body-parser');
const request = require('request-promise');
const fs = require('fs');

/* aqui eu faço a leitura do arquivo "dado.js" que está criado*/
var data = fs.readFileSync('dados.json');
/*quarda em um arquivo json*/
dados = JSON.parse(data);
/*exibi os dados, quando vc da o npm start*/
console.log(dados);

const app = express();
let cidades = [];
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var dados = [];
async function getTemperatura(cidades) {
  dados = [];
  for (var cidade of cidades) {
    var result = await request(
      `http://api.openweathermap.org/data/2.5/weather?q=${
        cidade.nome
      }&units=metric&appid=a6a0aff11936584d8e109d60151d9b84`
    );

    var temperatura_obj = JSON.parse(result);
    var temperatura = {
      cidade: cidade.nome,
      temperatura: temperatura_obj.main.temp,
      temp_max: temperatura_obj.main.temp_max,
      temp_min: temperatura_obj.main.temp_min
    };
    dados.push(temperatura);
  }
  var jsonData = JSON.stringify(dados);
  fs.writeFile('dados.json', jsonData, err => {
    if (err) {
      console.log(err);
    }
  });
  return dados;
}

app.get('/', (req, res) => {
  getTemperatura(cidades).then(result => {
    var data = { data: result };
    res.render('index', data);
  });
});

app.post('/', (req, res) => {
  cidade = {
    nome: req.body.nome
  };

  cidades.push(cidade);
  res.redirect('/');
});

app.listen(3000, () => console.log('Servidor rodando'));
